import { EdgeCollection } from 'arangojs/collection'
import { ensureEdgeIndexes_0_0_1 } from '../0.0.1/ensureEdgeIndexes0_0_1'

export const ensureEdgeIndexes_0_0_2 = async (edgeCollection: EdgeCollection) => {
  return ensureEdgeIndexes_0_0_1(edgeCollection)
}
