import Auth from '../lib/auth/types'

export type Context = {
  authSessionEnv: Auth.SessionEnv | null
}

export type RootValue = {}
