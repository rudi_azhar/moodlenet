import { Browser, BrowserProps } from '../../components/Browser/Browser'
import { CP, withCtrl } from '../../lib/ctrl'
import { HeaderPageTemplate, HeaderPageTemplateProps } from '../../templates/page/HeaderPageTemplate'
import './styles.scss'

export type SearchProps = {
  headerPageTemplateProps: CP<HeaderPageTemplateProps>
  browserProps: BrowserProps
}
export const Search = withCtrl<SearchProps>(({ headerPageTemplateProps, browserProps }) => {
  return (
    <HeaderPageTemplate {...headerPageTemplateProps}>
      <Browser {...browserProps} />
    </HeaderPageTemplate>
  )
})
Search.displayName = 'SearchPage'
