import Card from '../../../components/atoms/Card/Card'
import { Href, Link } from '../../../elements/link'
import { withCtrl } from '../../../lib/ctrl'
import defaultBackgroud from '../../../static/img/default-background.svg'
import '../../../styles/tags.css'
import './styles.scss'

export type ContributorCardProps = {
  avatarUrl: string | null
  displayName: string
  timeSinceCreation: string
  creatorProfileHref: Href
}

export const ContributorCard = withCtrl<ContributorCardProps>(
  ({ avatarUrl, displayName, timeSinceCreation, creatorProfileHref }) => {
    return (
      <Card className="contributor-card" hideBorderWhenSmall={true}>
        <Link href={creatorProfileHref}>
          <img className="avatar" src={avatarUrl || defaultBackgroud} alt="Avatar" />
        </Link>
        <div className="description">
          Uploaded {timeSinceCreation} by <Link href={creatorProfileHref}>{displayName}</Link>
        </div>
      </Card>
    )
  },
)
