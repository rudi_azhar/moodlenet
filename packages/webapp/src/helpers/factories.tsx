export const people: {displayName: string, avatarUrl: string, backgroundUrl: string, username: string, organization: string, location: string}[] = [
    {
        displayName: 'Juanito Rodriguez',
        avatarUrl: 'https://uifaces.co/our-content/donated/1H_7AxP0.jpg',
        backgroundUrl: 'https://upload.wikimedia.org/wikipedia/commons/3/37/Chualluma_La_Paz.jpg',
        organization: 'Universidad de la Paz',
        username: 'profesorrodriguez',
        location: 'Bolivia'
    },
    {
        displayName: 'Finaritra Randevoson',
        avatarUrl: 'https://images.pexels.com/photos/3746326/pexels-photo-3746326.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=200&w=200',
        backgroundUrl: 'https://www.tropenbos.org/image.php/2229/project/AdobeStock_301188707.jpeg?format=lightwindow',
        organization: 'University of Toamasina',
        username: 'finarandevoson',
        location: 'Madagascar'
    },
    {
        displayName: 'Alberto Curcella',
        avatarUrl: 'https://randomuser.me/api/portraits/men/4.jpg',
        backgroundUrl: 'https://media.fisheries.noaa.gov/dam-migration/vaquita_barbtaylorartist.jpg',
        organization: 'Univercità di Bologna',
        username: 'dralbicurcella',
        location: 'Italy'
    },
    {
        displayName: 'Katy Greentree',
        avatarUrl: 'https://images.pexels.com/photos/598745/pexels-photo-598745.jpeg?crop=faces&fit=crop&h=200&w=200&auto=compress&cs=tinysrgb',
        backgroundUrl: 'https://www.visitcos.com/images/made/images/remote/https_files.idssasp.com/public/C88/6abc181e-9791-4408-8a25-1002189f31bd/aeddeec0-d577-4485-9470-7bdcf6a1b779_820_450_70auto_s_c1.jpg',
        organization: 'University of Colorado Boulder',
        username: 'katygreetree',
        location: 'USA'
    },
]